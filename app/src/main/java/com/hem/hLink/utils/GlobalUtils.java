package com.hem.hLink.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.hem.hLink.interfaces.OnBackgroundTaskListener;
import com.hem.hLink.model.Response;

import java.io.IOException;

import retrofit2.adapter.rxjava.HttpException;

/**
 * Created by HEM on 2/7/17.
 */

public class GlobalUtils {
    public static void handleError(int errorCode, Object error, OnBackgroundTaskListener taskListener) {
        if (error instanceof HttpException) {
            Gson gson = new GsonBuilder().create();
            try {
                String errorBody = ((HttpException) error).response().errorBody().string();
                Response response = gson.fromJson(errorBody, Response.class);
                taskListener.handleError(errorCode, response.getMessage());

            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            taskListener.handleError(errorCode, "Network Error !");
        }
    }
}
