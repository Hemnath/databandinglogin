package com.hem.hLink.utils;

public class Constants {

    public static final String BASE_URL = "http://192.168.100.10:8080/api/v1/";
    public static final String TOKEN = "token";
    public static final String EMAIL = "email";
    public static final String FACEBOOK = "facebook";
    public static final String LOGIN = "login";
    public static final String LOGINTYPE = "logInType";
    public static final String PERSONANME = "personName";
    public static final String PERSONPHOTO = "personPhoto";

    public static final int errEmail = 2001;
    public static final int errPassword = 2002;
    public static final int errNewPassword = 2006;
    public static final int errEmailPassword = 2003;
    public static final int errName = 2004;
    public static final int errToken = 2005;

    public static final int msgEmailPassword = 3001;

    public static final int actionRegister = 1001;
    public static final int actionDialog = 1002;
    public static final int actionLogin = 1003;
    public static final int actionLogOut = 1004;
    public static final int actionLoginFacebook = 1005;
    public static final int actionLoginGoogle = 1006;


    public static final int resetPasswordInit = 5002;
    public static final int resetPasswordFinish = 5002;

    public static final int RC_SIGN_IN = 106;

}
