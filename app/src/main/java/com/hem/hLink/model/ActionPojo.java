package com.hem.hLink.model;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.hem.hLink.BR;
import com.hem.hLink.utils.Constants;

/**
 * Created by HEM on 2/7/17.
 */

public class ActionPojo extends BaseObservable {
    @Bindable
    private boolean loading;
    @Bindable
    private boolean init;
    private String loginType;

    public ActionPojo(boolean loading, boolean init, String loginType) {
        this.loading = loading;
        this.init = init;
        this.loginType = loginType;
    }

    public boolean isLoading() {
        return loading;
    }

    public void setLoading(boolean loading) {
        this.loading = loading;
        notifyPropertyChanged(BR.loading);
    }

    public boolean isInit() {
        return init;
    }

    public void setInit(boolean init) {
        this.init = init;
        notifyPropertyChanged(BR.loading);
    }

    public String getLoginType() {
        return loginType;
    }

    public void setLoginType(String loginType) {
        this.loginType = loginType;
    }

    @Bindable
    public boolean getStatus() {
        return !getLoginType().equals(Constants.LOGIN) || loading;
    }


}
