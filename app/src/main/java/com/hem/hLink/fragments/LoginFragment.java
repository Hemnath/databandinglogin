package com.hem.hLink.fragments;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hem.hLink.Handler.LoginHandler;
import com.hem.hLink.ProfileActivity;
import com.hem.hLink.R;
import com.hem.hLink.databinding.FragmentLoginBinding;
import com.hem.hLink.interfaces.OnBackgroundTaskListener;
import com.hem.hLink.interfaces.ViewActionListener;
import com.hem.hLink.model.ActionPojo;
import com.hem.hLink.model.Response;
import com.hem.hLink.model.User;
import com.hem.hLink.utils.Constants;

import rx.subscriptions.CompositeSubscription;


public class LoginFragment extends Fragment implements ViewActionListener, OnBackgroundTaskListener {

    public static final String TAG = LoginFragment.class.getSimpleName();

    private CompositeSubscription mSubscriptions;
    private SharedPreferences mSharedPreferences;
    private FragmentLoginBinding binding;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mSubscriptions = new CompositeSubscription();
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_login, container, false);
        binding.setLoginModel(new User());
        binding.setLoginActionModel(new ActionPojo(false, false,""));
        binding.setHandler(new LoginHandler(mSubscriptions, this, this));
        initSharedPreferences();
        binding.getLoginActionModel().setLoading(false);
        return binding.getRoot();
    }

    private void initSharedPreferences() {
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
    }

    private void showSnackBarMessage(String message) {
        if (getView() != null) {
            Snackbar.make(getView(), message, Snackbar.LENGTH_SHORT).show();
        }
    }

    private void goToRegister() {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        RegisterFragment fragment = new RegisterFragment();
        ft.replace(R.id.fragmentFrame, fragment, RegisterFragment.TAG);
        ft.commit();
    }

    private void showDialog() {
        ResetPasswordDialog fragment = new ResetPasswordDialog();
        fragment.show(getFragmentManager(), ResetPasswordDialog.TAG);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mSubscriptions.unsubscribe();
    }

    @Override
    public void onError(String msg, int code) {
        binding.getLoginActionModel().setLoading(false);
        switch (code) {
            case Constants.errEmail:
                binding.etEmail.setError(msg);
                break;
            case Constants.errPassword:
                binding.etPassword.setError(msg);
                break;
            case Constants.errEmailPassword:
                binding.etEmail.setError(msg);
                binding.etPassword.setError(msg);
                break;
        }
    }

    @Override
    public void onSetMassage(String msg, int code) {
        binding.getLoginActionModel().setLoading(false);
        showSnackBarMessage(msg);
    }

    @Override
    public void onSetAction(int code) {
        switch (code) {
            case Constants.actionRegister:
                goToRegister();
                break;
            case Constants.actionDialog:
                showDialog();
                break;
        }
    }

    @Override
    public void handleResponse(int successCode, Object result) {
        binding.getLoginActionModel().setLoading(false);
        Response response = (Response) result;
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(Constants.TOKEN, response.getToken());
        editor.putString(Constants.EMAIL, response.getMessage());
        editor.putString(Constants.LOGINTYPE, Constants.LOGIN);
        editor.apply();

        binding.etEmail.setText(null);
        binding.etPassword.setText(null);

        Intent intent = new Intent(getActivity(), ProfileActivity.class);
        startActivity(intent);
    }

    @Override
    public void handleError(int errorCode, String error) {
        binding.getLoginActionModel().setLoading(false);
        showSnackBarMessage(error);
    }
}
