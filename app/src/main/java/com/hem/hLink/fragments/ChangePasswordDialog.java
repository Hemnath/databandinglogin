package com.hem.hLink.fragments;

import android.app.DialogFragment;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hem.hLink.Handler.ChangePasswordHandler;
import com.hem.hLink.ProfileActivity;
import com.hem.hLink.R;
import com.hem.hLink.databinding.DialogChangePasswordBinding;
import com.hem.hLink.interfaces.OnBackgroundTaskListener;
import com.hem.hLink.interfaces.ViewActionListener;
import com.hem.hLink.model.ActionPojo;
import com.hem.hLink.model.User;
import com.hem.hLink.utils.Constants;

import rx.subscriptions.CompositeSubscription;


public class ChangePasswordDialog extends DialogFragment implements ViewActionListener, OnBackgroundTaskListener {

    public static final String TAG = ChangePasswordDialog.class.getSimpleName();
    private CompositeSubscription mSubscriptions;
    private Listener mListener;
    private DialogChangePasswordBinding binding;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mSubscriptions = new CompositeSubscription();
        binding = DataBindingUtil.inflate(inflater, R.layout.dialog_change_password, container, false);
        binding.setUserModel(new User());
        binding.setActionModel(new ActionPojo(false, true,""));
        binding.setHandler(new ChangePasswordHandler(mSubscriptions, this, this));
        getData();
        return binding.getRoot();
    }

    private void getData() {
        Bundle bundle = getArguments();
        String mToken = bundle.getString(Constants.TOKEN);
        String mEmail = bundle.getString(Constants.EMAIL);
        binding.getUserModel().setToken(mToken);
        binding.getUserModel().setEmail(mEmail);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mListener = (ProfileActivity) context;
    }

    private void setError() {
        binding.tiOldPassword.setError(null);
        binding.tiNewPassword.setError(null);
    }

    private void showMessage(String message) {
        binding.tvMessage.setVisibility(View.VISIBLE);
        binding.tvMessage.setText(message);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mSubscriptions.unsubscribe();
    }

    @Override
    public void handleResponse(int successCode, Object result) {
        binding.getActionModel().setLoading(false);
        mListener.onPasswordChanged();
        dismiss();
    }

    @Override
    public void handleError(int errorCode, String error) {
        binding.getActionModel().setLoading(false);
        showMessage(error);
    }

    @Override
    public void onError(String msg, int code) {
        binding.getActionModel().setLoading(false);
        switch (code) {
            case Constants.errPassword:
                binding.tiOldPassword.setError(msg);
                break;
            case Constants.errNewPassword:
                binding.tiNewPassword.setError(msg);
                break;
            case Constants.errEmailPassword:
                setError();
                break;
        }
    }

    @Override
    public void onSetMassage(String msg, int code) {
        binding.getActionModel().setLoading(false);
        showMessage(msg);
    }

    @Override
    public void onSetAction(int code) {
        dismiss();
    }

    public interface Listener {

        void onPasswordChanged();
    }
}
