package com.hem.hLink.fragments;


import android.app.DialogFragment;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hem.hLink.Handler.ResetPasswordHandler;
import com.hem.hLink.LoginActivity;
import com.hem.hLink.R;
import com.hem.hLink.databinding.DialogResetPasswordBinding;
import com.hem.hLink.interfaces.OnBackgroundTaskListener;
import com.hem.hLink.interfaces.ViewActionListener;
import com.hem.hLink.model.ActionPojo;
import com.hem.hLink.model.Response;
import com.hem.hLink.model.User;
import com.hem.hLink.utils.Constants;

import rx.subscriptions.CompositeSubscription;

public class ResetPasswordDialog extends DialogFragment implements ViewActionListener, OnBackgroundTaskListener {

    public static final String TAG = ResetPasswordDialog.class.getSimpleName();

    private CompositeSubscription mSubscriptions;
    private Listener mListner;
    private DialogResetPasswordBinding binding;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mSubscriptions = new CompositeSubscription();
        binding = DataBindingUtil.inflate(inflater, R.layout.dialog_reset_password, container, false);
        binding.setUserModel(new User());
        binding.setActionModel(new ActionPojo(false, true,""));
        binding.setHandler(new ResetPasswordHandler(mSubscriptions, this, this));
        return binding.getRoot();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mListner = (LoginActivity) context;
    }

    private void setEmptyFields() {
        binding.tiEmail.setError(null);
        binding.tiToken.setError(null);
        binding.tiPassword.setError(null);
        binding.tvMessage.setText(null);
        binding.tvMessage.setVisibility(View.GONE);
    }

    public void setToken(String token) {
        binding.etToken.setText(token);
    }

    private void showMessage(String message) {
        binding.tvMessage.setVisibility(View.VISIBLE);
        binding.tvMessage.setText(message);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mSubscriptions.unsubscribe();
    }

    @Override
    public void handleResponse(int successCode, Object result) {
        binding.getActionModel().setLoading(false);
        Response response = (Response) result;
        if (Constants.resetPasswordInit == successCode) {
            binding.getActionModel().setInit(false);
            showMessage(response.getMessage());
        } else {
            mListner.onPasswordReset(response.getMessage());
            dismiss();
        }
    }

    @Override
    public void handleError(int errorCode, String error) {
        binding.getActionModel().setLoading(false);
        showMessage(error);
    }
    @Override
    public void onError(String msg, int code) {
        binding.getActionModel().setLoading(false);
        switch (code) {
            case Constants.errEmail:
                binding.tiEmail.setError(msg);
                break;
            case Constants.errPassword:
                binding.tiPassword.setError(msg);
                break;
            case Constants.errToken:
                binding.tiToken.setError(msg);
                break;
            case Constants.errEmailPassword:
                setEmptyFields();
                break;
        }
    }

    @Override
    public void onSetMassage(String msg, int code) {
        binding.getActionModel().setLoading(false);
        showMessage(msg);
    }

    @Override
    public void onSetAction(int code) {

    }

    public interface Listener {
        void onPasswordReset(String message);
    }
}
