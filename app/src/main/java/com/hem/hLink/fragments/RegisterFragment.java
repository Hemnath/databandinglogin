package com.hem.hLink.fragments;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.hem.hLink.Handler.RegisterHandler;
import com.hem.hLink.ProfileActivity;
import com.hem.hLink.R;
import com.hem.hLink.databinding.FragmentRegisterBinding;
import com.hem.hLink.interfaces.OnBackgroundTaskListener;
import com.hem.hLink.interfaces.ViewActionListener;
import com.hem.hLink.model.ActionPojo;
import com.hem.hLink.model.Response;
import com.hem.hLink.model.User;
import com.hem.hLink.utils.Constants;

import rx.subscriptions.CompositeSubscription;

import static com.hem.hLink.utils.Constants.RC_SIGN_IN;


public class RegisterFragment extends Fragment implements ViewActionListener, OnBackgroundTaskListener, GoogleApiClient.OnConnectionFailedListener {

    public static final String TAG = RegisterFragment.class.getSimpleName();

    private CompositeSubscription mSubscriptions;
    private SharedPreferences mSharedPreferences;
    private FragmentRegisterBinding binding;
    private GoogleApiClient mGoogleApiClient;
    private AccessToken accessToken;
    private AccessTokenTracker accessTokenTracker;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mSubscriptions = new CompositeSubscription();
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_register, container, false);
        binding.setUserModel(new User());
        binding.setActionModel(new ActionPojo(false, false, ""));
        binding.setHandler(new RegisterHandler(mSubscriptions, this, this));
        binding.getActionModel().setLoading(false);
        initLogin();
        initSharedPreferences();
        return binding.getRoot();
    }


    void initLogin() {

        FacebookSdk.sdkInitialize(getActivity());
        AppEventsLogger.activateApp(getActivity());

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .enableAutoManage((FragmentActivity) getActivity(), this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
    }

    private void initSharedPreferences() {
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
    }

    void onFacebookClick() {
        CallbackManager callbackManager = CallbackManager.Factory.create();
        binding.btnFacebook.setReadPermissions("email");
        binding.btnFacebook.setFragment(this);

        binding.btnFacebook.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Intent intent = new Intent(getActivity(), ProfileActivity.class);
                getActivity().startActivity(intent);
                getActivity().finish();
            }

            @Override
            public void onCancel() {
                showSnackBarMessage("Login Failed!");
            }

            @Override
            public void onError(FacebookException exception) {
                showSnackBarMessage("Login Failed!");
            }
        });

    }

    private void signIn() {
        binding.getActionModel().setLoading(true);
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void setError() {
        binding.tiName.setError(null);
        binding.tiEmail.setError(null);
        binding.tiPassword.setError(null);
    }

    private void showSnackBarMessage(String message) {
        if (getView() != null) {
            Snackbar.make(getView(), message, Snackbar.LENGTH_SHORT).show();
        }
    }

    private void goToLogin() {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        LoginFragment fragment = new LoginFragment();
        ft.replace(R.id.fragmentFrame, fragment, LoginFragment.TAG);
        ft.commit();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mSubscriptions.unsubscribe();
        mGoogleApiClient.stopAutoManage((FragmentActivity) getActivity());
        mGoogleApiClient.disconnect();
    }

    @Override
    public void handleResponse(int successCode, Object result) {
        Response response = (Response) result;
        binding.getActionModel().setLoading(false);
        showSnackBarMessage(response.getMessage());
    }

    @Override
    public void handleError(int errorCode, String error) {
        binding.getActionModel().setLoading(false);
        showSnackBarMessage(error);
    }

    @Override
    public void onError(String msg, int code) {
        setError();
        binding.getActionModel().setLoading(false);
        switch (code) {
            case Constants.errEmail:
                binding.tiEmail.setError(msg);
                break;
            case Constants.errPassword:
                binding.tiPassword.setError(msg);
                break;
            case Constants.errName:
                binding.tiName.setError(msg);
                break;
        }
    }

    @Override
    public void onSetMassage(String msg, int code) {
        binding.getActionModel().setLoading(false);
        showSnackBarMessage(msg);
    }

    @Override
    public void onSetAction(int code) {
        switch (code) {
            case Constants.actionLogin:
                goToLogin();
                break;
            case Constants.actionLoginFacebook:
                onFacebookClick();
                break;
            case Constants.actionLoginGoogle:
                signIn();
                break;
        }

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d(TAG, "handleSignInResult:" + "Failed");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        binding.getActionModel().setLoading(false);
        if (requestCode == RC_SIGN_IN && resultCode != 0) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }


    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.d(TAG, "handleSignInResult:" + result.isSuccess());

        GoogleSignInAccount acct = result.getSignInAccount();
        if (acct != null) {
            SharedPreferences.Editor editor = mSharedPreferences.edit();
            editor.putString(Constants.TOKEN, acct.getId());
            editor.putString(Constants.EMAIL, acct.getEmail());
            editor.putString(Constants.PERSONANME, acct.getDisplayName());
            editor.putString(Constants.PERSONPHOTO, String.valueOf(acct.getPhotoUrl()));
            editor.putString(Constants.LOGINTYPE, Constants.EMAIL);
            editor.apply();
        }
        Intent intent = new Intent(getActivity(), ProfileActivity.class);
        getActivity().startActivity(intent);
        getActivity().finish();
    }

}
