package com.hem.hLink.interfaces;

/**
 * Created by HEM on 12/5/16.
 */

public interface AlertDialogListener {

    void onAlertSuccess();

    void onAlertFailed();
}
