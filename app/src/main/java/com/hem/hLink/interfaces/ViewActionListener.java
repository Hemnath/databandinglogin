package com.hem.hLink.interfaces;

/**
 * Created by HEM on 2/6/17.
 */

public interface ViewActionListener {
    void onError(String msg, int code);

    void onSetMassage(String msg, int code);

    void onSetAction(int code);
}
