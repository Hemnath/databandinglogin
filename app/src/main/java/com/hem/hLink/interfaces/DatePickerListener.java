package com.hem.hLink.interfaces;

/**
 * Created by HEM on 12/13/16.
 */

public interface DatePickerListener {
    void onDatePick(String date, int viewType);
}
