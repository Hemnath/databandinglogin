package com.hem.hLink.interfaces;

/**
 * Created by HEM on 12/21/16.
 */

public interface OnBackgroundTaskListener<U> {
    void handleResponse(int successCode, U result);

    void handleError(int errorCode, String error);
}