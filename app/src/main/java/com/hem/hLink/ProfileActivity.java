package com.hem.hLink;

import android.content.Intent;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.hem.hLink.Handler.ProfileHandler;
import com.hem.hLink.databinding.ActivityProfileBinding;
import com.hem.hLink.fragments.ChangePasswordDialog;
import com.hem.hLink.interfaces.OnBackgroundTaskListener;
import com.hem.hLink.interfaces.ViewActionListener;
import com.hem.hLink.model.ActionPojo;
import com.hem.hLink.model.User;
import com.hem.hLink.utils.Constants;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.util.Date;

import rx.subscriptions.CompositeSubscription;

public class ProfileActivity extends AppCompatActivity implements ChangePasswordDialog.Listener, ViewActionListener, OnBackgroundTaskListener, GoogleApiClient.OnConnectionFailedListener {

    public static final String TAG = ProfileActivity.class.getSimpleName();
    String logintype;
    private SharedPreferences mSharedPreferences;
    private String mToken;
    private String mEmail;
    private String mName;

    private CompositeSubscription mSubscriptions;
    private ActivityProfileBinding binding;
    private ImageLoaderConfiguration config;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSubscriptions = new CompositeSubscription();
        binding = DataBindingUtil.setContentView(this, R.layout.activity_profile);
        binding.setUserModel(new User());
        binding.setActionModel(new ActionPojo(false, false, ""));
        binding.setHandler(new ProfileHandler(mSubscriptions, this, this));
        initSharedPreferences();
        init();
    }

    private void initSharedPreferences() {
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        mToken = mSharedPreferences.getString(Constants.TOKEN, "");
        mEmail = mSharedPreferences.getString(Constants.EMAIL, "");
        mName = mSharedPreferences.getString(Constants.PERSONANME, "");
        logintype = mSharedPreferences.getString(Constants.LOGINTYPE, "");
        binding.getActionModel().setLoginType(logintype);
    }

    void init() {
        User user = binding.getUserModel();
        user.setEmail(mEmail);
        user.setName(mName);
        user.setToken(mToken);
        user.setCreated_at(String.valueOf(new Date()));
        imageLoaderConfiguration();
        switch (logintype) {
            case Constants.EMAIL:
                initWithGoogle();
                binding.btnChangePassword.setEnabled(false);
                break;
            case Constants.FACEBOOK:
                initWithFacebook();
                break;
            default:
                initWithApi();
                break;
        }
    }

    void imageLoaderConfiguration() {
        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                .cacheOnDisk(true).cacheInMemory(true)
                .imageScaleType(ImageScaleType.EXACTLY)
                .displayer(new FadeInBitmapDisplayer(300)).build();

        config = new ImageLoaderConfiguration.Builder(
                getApplicationContext())
                .defaultDisplayImageOptions(defaultOptions)
                .memoryCache(new WeakMemoryCache())
                .diskCacheSize(100 * 1024 * 1024).build();


    }

    void initWithGoogle() {
        binding.getActionModel().setLoading(true);
        String imageUri = mSharedPreferences.getString(Constants.PERSONPHOTO, "");
        ImageLoader imageLoader = ImageLoader.getInstance();
        imageLoader.init(config);

        imageLoader.displayImage(imageUri, binding.personPhoto);
        imageLoader.loadImage(imageUri, new SimpleImageLoadingListener() {
            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                binding.getActionModel().setLoading(false);
            }
        });
    }

    void initWithFacebook() {
        binding.getActionModel().setLoading(true);
    }

    void initWithApi() {
        binding.getActionModel().setLoading(true);
        binding.getHandler().loadProfile(mToken, mEmail);
    }

    private void logout() {
        clearSharedPreferences();
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }


    private void clearSharedPreferences() {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.clear();
        editor.apply();
    }


    private void showDialog() {
        ChangePasswordDialog fragment = new ChangePasswordDialog();
        Bundle bundle = new Bundle();
        bundle.putString(Constants.EMAIL, mEmail);
        bundle.putString(Constants.TOKEN, mToken);
        fragment.setArguments(bundle);
        fragment.show(getFragmentManager(), ChangePasswordDialog.TAG);
    }


    private void showSnackBarMessage(String message) {
        Snackbar.make(findViewById(R.id.activity_profile), message, Snackbar.LENGTH_SHORT).show();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mSubscriptions.unsubscribe();
    }

    @Override
    public void onPasswordChanged() {
        showSnackBarMessage("Password Changed Successfully !");
    }

    @Override
    public void handleResponse(int successCode, Object result) {
        binding.getActionModel().setLoading(false);
        User user = (User) result;
        binding.tvName.setText(user.getName());
        binding.tvEmail.setText(user.getEmail());
        binding.tvDate.setText(user.getCreated_at());
    }

    @Override
    public void handleError(int errorCode, String error) {
        binding.getActionModel().setLoading(false);
        showSnackBarMessage(error);
    }

    @Override
    public void onError(String msg, int code) {

    }

    @Override
    public void onSetMassage(String msg, int code) {
        binding.getActionModel().setLoading(false);
        showSnackBarMessage(msg);
    }

    @Override
    public void onSetAction(int code) {
        switch (code) {
            case Constants.actionLogOut:
                logout();
                break;
            case Constants.actionDialog:
                showDialog();
                break;
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}

