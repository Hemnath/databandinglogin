package com.hem.hLink.Handler;

import android.view.View;

import com.hem.hLink.interfaces.OnBackgroundTaskListener;
import com.hem.hLink.interfaces.ViewActionListener;
import com.hem.hLink.model.ActionPojo;
import com.hem.hLink.model.Response;
import com.hem.hLink.model.User;
import com.hem.hLink.network.NetworkUtil;
import com.hem.hLink.utils.Constants;
import com.hem.hLink.utils.GlobalUtils;

import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

import static com.hem.hLink.utils.Validation.validateFields;

/**
 * Created by HEM on 2/7/17.
 */

public class ChangePasswordHandler {
    private ViewActionListener actionListener;
    private OnBackgroundTaskListener taskListener;
    private CompositeSubscription mSubscriptions;

    public ChangePasswordHandler(CompositeSubscription mSubscriptions, ViewActionListener actionListener, OnBackgroundTaskListener taskListener) {
        this.actionListener = actionListener;
        this.taskListener = taskListener;
        this.mSubscriptions = mSubscriptions;

    }

    public View.OnClickListener changePassword(final User user, final ActionPojo actionPojo) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setError();
                actionPojo.setLoading(true);
                rchangePasswordFinish(user);
            }
        };

    }
    public View.OnClickListener cancel() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                actionListener.onSetAction(0);
            }
        };

    }

    private void rchangePasswordFinish(User user) {
        String oldPassword = user.getPassword();
        String newPassword = user.getNewPassword();

        int err = 0;

        if (!validateFields(oldPassword)) {
            err++;
            actionListener.onError("Password Should not be empty !", Constants.errPassword);
        }

        if (!validateFields(newPassword)) {
            err++;
            actionListener.onError("Password Should not be empty !", Constants.errNewPassword);
        }

        if (err == 0) {
            changePasswordProgress(user);
        }
    }

    private void setError() {
        actionListener.onError(null, Constants.errEmailPassword);
    }


    private void changePasswordProgress(User user) {
        mSubscriptions.add(NetworkUtil.getRetrofit(user.getToken()).changePassword(user.getEmail(), user)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Action1<Response>() {
                    @Override
                    public void call(Response response) {
                        taskListener.handleResponse(0, response);
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        GlobalUtils.handleError(0, throwable, taskListener);
                    }
                })
        );
    }
}