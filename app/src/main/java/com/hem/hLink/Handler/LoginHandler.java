package com.hem.hLink.Handler;

import android.view.View;

import com.hem.hLink.interfaces.OnBackgroundTaskListener;
import com.hem.hLink.interfaces.ViewActionListener;
import com.hem.hLink.model.ActionPojo;
import com.hem.hLink.model.Response;
import com.hem.hLink.model.User;
import com.hem.hLink.network.NetworkUtil;
import com.hem.hLink.utils.Constants;
import com.hem.hLink.utils.GlobalUtils;

import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

import static com.hem.hLink.utils.Validation.validateEmail;
import static com.hem.hLink.utils.Validation.validateFields;

/**
 * Created by HEM on 2/6/17.
 */

public class LoginHandler {

    private ViewActionListener actionListener;
    private OnBackgroundTaskListener taskListener;
    private CompositeSubscription mSubscriptions;

    public LoginHandler(CompositeSubscription mSubscriptions, ViewActionListener actionListener, OnBackgroundTaskListener taskListener) {
        this.actionListener = actionListener;
        this.taskListener = taskListener;
        this.mSubscriptions = mSubscriptions;

    }

    public View.OnClickListener login(final User user , final ActionPojo actionPojo) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setError();
                actionPojo.setLoading(true);
                String email = user.getEmail();
                String password = user.getPassword();
                int err = 0;
                if (!validateEmail(email)) {
                    err++;
                    actionListener.onError("Email should be valid !", Constants.errEmail);
                }

                if (!validateFields(password)) {
                    err++;
                    actionListener.onError("Password should not be empty !", Constants.errPassword);
                }

                if (err == 0) {
                    loginProcess(email, password);
                } else {
                    actionListener.onSetMassage("Enter Valid Details !", Constants.msgEmailPassword);
                }
            }
        };

    }

    private void setError() {
        actionListener.onError(null, Constants.errEmailPassword);
    }

    private void loginProcess(String email, String password) {

        mSubscriptions.add(NetworkUtil.getRetrofit(email, password).login()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Action1<Response>() {
                    @Override
                    public void call(Response response) {
                        taskListener.handleResponse(0, response);
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        GlobalUtils.handleError(0, throwable, taskListener);
                    }
                })
        );
    }

    public View.OnClickListener goToRegister() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                actionListener.onSetAction(Constants.actionRegister);
            }
        };
    }

    public View.OnClickListener showDialog() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                actionListener.onSetAction(Constants.actionDialog);
            }
        };
    }
}
