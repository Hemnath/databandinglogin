package com.hem.hLink.Handler;

import android.view.View;

import com.hem.hLink.interfaces.OnBackgroundTaskListener;
import com.hem.hLink.interfaces.ViewActionListener;
import com.hem.hLink.model.ActionPojo;
import com.hem.hLink.model.Response;
import com.hem.hLink.model.User;
import com.hem.hLink.network.NetworkUtil;
import com.hem.hLink.utils.Constants;
import com.hem.hLink.utils.GlobalUtils;

import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

import static com.hem.hLink.utils.Validation.validateEmail;
import static com.hem.hLink.utils.Validation.validateFields;

/**
 * Created by HEM on 2/7/17.
 */

public class ResetPasswordHandler {

    private ViewActionListener actionListener;
    private OnBackgroundTaskListener taskListener;
    private CompositeSubscription mSubscriptions;

    public ResetPasswordHandler(CompositeSubscription mSubscriptions, ViewActionListener actionListener, OnBackgroundTaskListener taskListener) {
        this.actionListener = actionListener;
        this.taskListener = taskListener;
        this.mSubscriptions = mSubscriptions;

    }

    public View.OnClickListener resetPassword(final User user, final ActionPojo actionPojo) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setError();
                actionPojo.setLoading(true);
                if (actionPojo.isInit()) resetPasswordInit(user);
                else resetPasswordFinish(user);
            }
        };

    }

    private void resetPasswordInit(User user) {
        String mEmail = user.getEmail();
        int err = 0;
        if (!validateEmail(mEmail)) {
            err++;
            actionListener.onError("Email Should be Valid !", Constants.errEmail);
        }
        if (err == 0) {
            resetPasswordInitProgress(mEmail);
        }
    }

    private void resetPasswordFinish(User user) {
        String token = user.getToken();
        String password = user.getPassword();
        int err = 0;
        if (!validateFields(token)) {
            err++;
            actionListener.onError("Token Should not be empty !", Constants.errToken);
        }

        if (!validateFields(password)) {
            err++;
            actionListener.onError("Password Should not be empty !", Constants.errPassword);
        }
        if (err == 0) {
            User user1 = new User();
            user1.setPassword(password);
            user1.setToken(token);
            resetPasswordFinishProgress(user1);
        }
    }

    private void setError() {
        actionListener.onError(null, Constants.errEmailPassword);
    }


    private void resetPasswordInitProgress(String email) {

        mSubscriptions.add(NetworkUtil.getRetrofit().resetPasswordInit(email)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Action1<Response>() {
                    @Override
                    public void call(Response response) {
                        taskListener.handleResponse(Constants.resetPasswordInit, response);
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        GlobalUtils.handleError(0, throwable, taskListener);
                    }
                })
        );
    }

    private void resetPasswordFinishProgress(User user) {

        mSubscriptions.add(NetworkUtil.getRetrofit().resetPasswordFinish(user.getEmail(), user)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Action1<Response>() {
                    @Override
                    public void call(Response response) {
                        taskListener.handleResponse(Constants.resetPasswordFinish, response);
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        GlobalUtils.handleError(0, throwable, taskListener);
                    }
                })
        );
    }
}
