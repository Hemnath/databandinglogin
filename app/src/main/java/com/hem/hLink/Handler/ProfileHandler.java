package com.hem.hLink.Handler;

import android.view.View;

import com.hem.hLink.interfaces.OnBackgroundTaskListener;
import com.hem.hLink.interfaces.ViewActionListener;
import com.hem.hLink.model.User;
import com.hem.hLink.network.NetworkUtil;
import com.hem.hLink.utils.Constants;
import com.hem.hLink.utils.GlobalUtils;

import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by HEM on 2/7/17.
 */

public class ProfileHandler {
    private ViewActionListener actionListener;
    private OnBackgroundTaskListener taskListener;
    private CompositeSubscription mSubscriptions;

    public ProfileHandler(CompositeSubscription mSubscriptions, ViewActionListener actionListener, OnBackgroundTaskListener taskListener) {
        this.actionListener = actionListener;
        this.taskListener = taskListener;
        this.mSubscriptions = mSubscriptions;

    }

    public void loadProfile(String mToken, String mEmail) {

        mSubscriptions.add(NetworkUtil.getRetrofit(mToken).getProfile(mEmail)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Action1<User>() {
                    @Override
                    public void call(User response) {
                        taskListener.handleResponse(0, response);
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        GlobalUtils.handleError(0, throwable, taskListener);
                    }
                })
        );
    }

    public View.OnClickListener logout() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                actionListener.onSetAction(Constants.actionLogOut);
            }
        };
    }

    public View.OnClickListener showDialog() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                actionListener.onSetAction(Constants.actionDialog);
            }
        };
    }
}
