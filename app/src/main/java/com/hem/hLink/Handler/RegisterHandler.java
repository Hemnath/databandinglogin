package com.hem.hLink.Handler;

import android.view.View;

import com.hem.hLink.interfaces.OnBackgroundTaskListener;
import com.hem.hLink.interfaces.ViewActionListener;
import com.hem.hLink.model.ActionPojo;
import com.hem.hLink.model.Response;
import com.hem.hLink.model.User;
import com.hem.hLink.network.NetworkUtil;
import com.hem.hLink.utils.Constants;
import com.hem.hLink.utils.GlobalUtils;

import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

import static com.hem.hLink.utils.Validation.validateEmail;
import static com.hem.hLink.utils.Validation.validateFields;

/**
 * Created by HEM on 2/7/17.
 */

public class RegisterHandler {

    private ViewActionListener actionListener;
    private OnBackgroundTaskListener taskListener;
    private CompositeSubscription mSubscriptions;


    public RegisterHandler(CompositeSubscription mSubscriptions, ViewActionListener actionListener, OnBackgroundTaskListener taskListener) {
        this.actionListener = actionListener;
        this.taskListener = taskListener;
        this.mSubscriptions = mSubscriptions;
    }

    public View.OnClickListener register(final User user, final ActionPojo actionPojo) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setError();
                actionPojo.setLoading(true);
                String name = user.getName();
                String email = user.getEmail();
                String password = user.getPassword();

                int err = 0;
                if (!validateFields(name)) {
                    err++;
                    actionListener.onError("Name should not be empty !", 0);
                }

                if (!validateEmail(email)) {
                    err++;
                    actionListener.onError("Email should be valid !", Constants.errEmail);
                }

                if (!validateFields(password)) {
                    err++;
                    actionListener.onError("Password should not be empty !", Constants.msgEmailPassword);
                }

                if (err == 0) {
                    User user = new User();
                    user.setName(name);
                    user.setEmail(email);
                    user.setPassword(password);
                    registerProcess(user);

                } else {
                    actionListener.onSetMassage("Enter Valid Details !", Constants.msgEmailPassword);
                }
            }
        };
    }

    public View.OnClickListener onClickfacebook() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                actionListener.onSetAction(Constants.actionLoginFacebook);
            }
        };
    }
 public View.OnClickListener onClickGoogle() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                actionListener.onSetAction(Constants.actionLoginGoogle);
            }
        };
    }


    private void setError() {
        actionListener.onError(null, Constants.errEmailPassword);
    }

    private void registerProcess(User user) {
        mSubscriptions.add(NetworkUtil.getRetrofit().register(user)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Action1<Response>() {
                    @Override
                    public void call(Response response) {
                        taskListener.handleResponse(0, response);
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        GlobalUtils.handleError(0, throwable, taskListener);
                    }
                })
        );
    }

    public View.OnClickListener goToLogin() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                actionListener.onSetAction(Constants.actionLogin);
            }
        };
    }
}
